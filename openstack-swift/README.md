# Openstack SWIFT
## Deployment instructions
https://github.com/NVIDIA/docker-swift
## API reference
https://docs.openstack.org/api-ref/object-store/index.html?expanded=create-update-or-delete-account-metadata-detail#containers
## Ports
    8081 - Nexus
    8082 - Jenkins
    8090 - Gatsby
    8095 - Spring
    8096 - Openstack Swift
    8097 - MongoDB
    8100 - nginx
    9000 - Sonarqube